﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio04
{
    public class Objeto
    {
        public int Valor { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Filtrar una lista de objetos por un rango de valores de una propiedad numerica
            //definir rango
            List<Objeto> objetos = new List<Objeto>
            {
                //lista de e objetos
            new Objeto { Valor = 12 },
            new Objeto { Valor = 15 },
            new Objeto { Valor = 35 },
            new Objeto { Valor = 40 },
            new Objeto { Valor = 46 }
            };
            //rango de valores
            int valorMinimo = 15;
            int valorMaximo = 35;
            //filtrar lisa de objetos
            var objetosFiltrados = objetos.Where(o => o.Valor >= valorMinimo && o.Valor <= valorMaximo).ToList();
            //  imprimir resultado
            Console.WriteLine("Objetos con valor entre " + valorMinimo + " y " + valorMaximo + ":");
            foreach (var objeto in objetosFiltrados)
            {
                Console.WriteLine("Valor: " + objeto.Valor);
            }
        }
    }
}
