﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio03
{
   public  class Producto
    {
        //definir una clases para representar 
        public string Nombre { get; set; }

        public double Precio { get; set; }
    }

   public class Program
    {
        static void Main(string[] args)
        {
            //Filtrar una lisa de objetos por un rango e valores de una propiedad numérica
            List<Producto> productos = new List<Producto>
        {
   //crear lista
            new Producto { Nombre = "Producto1", Precio = 12 },
            new Producto { Nombre = "Producto2", Precio = 15 },
            new Producto { Nombre = "Producto3", Precio = 35 },
            new Producto { Nombre = "Producto4", Precio = 40 },
            new Producto { Nombre = "Producto5", Precio = 46 }
        };
            //definir los rangos de valores
            int precioMinimo = 15;
            int precioMaximo = 35;
         //filtar lista de los objetos
            var productosFiltrados = productos.Where(p => p.Precio >= precioMinimo && p.Precio <= precioMaximo).ToList();
            //imprimir los resultados
            Console.WriteLine("Productos con precio entre " + precioMinimo + " y " + precioMaximo + ":");
            foreach (var producto in productosFiltrados)
            {
                Console.WriteLine(producto.Nombre + ": " + producto.Precio);
            }

        }
    }
}
