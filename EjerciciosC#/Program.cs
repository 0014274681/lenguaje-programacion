﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosC_
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Concatenar dos listas eliminano duplicados
            //primera lista
            string Paul = null;
            string Paty = null;
            string Josue = null;
            string Carlos = null;
            List<string> lista1 = new List<string> { Carlos, Paul, Paty, Josue };
            //segunda lista
            string Carla = null;
            string Alexis = null;
            string Carmen = null;
            string Paola = null;
            List<string> lista2 = new List<string> { Paola , Carla, Alexis, Carmen };

            //concatenar lista y eliminar duplicado
            var listaConcatenada = lista1.Concat(lista2).Distinct();
         
            //imprimimos el resultado
            Console.WriteLine("Lista Concatenada y sin duplicados:");
            foreach (string elemento in listaConcatenada)
            {
                Console.WriteLine(elemento);
            }
        }
    }
}
