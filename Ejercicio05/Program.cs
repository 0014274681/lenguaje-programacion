﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio05
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Encuentra la moda en una lista de números

            List<int> numeros = new List<int> { 1, 2, 3, 3, 3, 4, 4, 5, 5, 5, 5 };
            int moda = numeros.GroupBy(n => n)
                             .OrderByDescending(g => g.Count())
                             .Select(g => g.Key)
                             .First();
            Console.WriteLine("La moda es: " + moda);
        }
    }
}
