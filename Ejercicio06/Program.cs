﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio06
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<int> lista = new List<int> { 1, 2, 2, 3, 4, 4, 5, 6, 6, 6 };

            List<int> listaSinDuplicados = new List<int>();
            foreach (int elemento in lista)
            {
                if (!listaSinDuplicados.Contains(elemento))
                {
                    listaSinDuplicados.Add(elemento);
                }
            }
            Console.WriteLine("Lista sin duplicados:");
            foreach (int elemento in listaSinDuplicados)
            {
                Console.WriteLine(elemento);
            }
        }
    }
}
