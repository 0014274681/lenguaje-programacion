﻿namespace Program
{
    internal class Producto
    {
        public string Nombre { get; internal set; }
        public int Precio { get; internal set; }
    }
}