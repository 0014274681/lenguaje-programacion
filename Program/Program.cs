﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ejercicio04;
using Program;

namespace Program
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Ejercicio01();
            Ejercicio02();
            Ejercicio03();
            Ejercicio04();
            Ejercicio05();

            Console.WriteLine("preciona cualquier tecla para salir");
            Console.ReadKey();
        }

        //concatenar dos listas eliminando duplicados
        static void Ejercicio01()
        {
            Console.WriteLine("Ejercicio 01:");

            string Paul = null;
            string Paty = null;
            string Josue = null;
            string Carlos = null;
            List<string> lista1 = new List<string> { Carlos, Paul, Paty, Josue };
            string Carla = null;
            string Alexis = null;
            string Carmen = null;
            List<string> lista2 = new List<string> { Paul, Carla, Alexis, Carmen };

            List<string> listaConcatenada = new List<string>(lista1);
            listaConcatenada.AddRange(lista2);

            HashSet<string> conjunto = new HashSet<string>(listaConcatenada);
            listaConcatenada.Clear();
            listaConcatenada.AddRange(conjunto);

            Console.WriteLine("Lista Concatenada y sin duplicados:");
            foreach (string elemento in listaConcatenada)
            {
                Console.WriteLine(elemento);
            }
        }


        static void Ejercicio02()
        {
            //calcular la summa de los cuadrados de los numeros en una lista.
            Console.WriteLine("Ejercicio 02:");
            List<Producto> productos = new List<Producto>
        {
            new Producto { Nombre = "Producto1", Precio = 12 },
            new Producto { Nombre = "Producto2", Precio = 15 },
            new Producto { Nombre = "Producto3", Precio = 35 },
            new Producto { Nombre = "Producto4", Precio = 40 },
            new Producto { Nombre = "Producto5", Precio = 46 }
        };

            double precioMinimo = 15;
            double precioMaximo = 35;
            var productosFiltrados = productos.Where(p => p.Precio >= precioMinimo && p.Precio <= precioMaximo).ToList();
            Console.WriteLine("Productos con precio entre " + precioMinimo + " y " + precioMaximo + ":");
            foreach (var producto in productosFiltrados)
            {
                Console.WriteLine(producto.Nombre + ": " + producto.Precio);
            }
        }
        static void Ejercicio03()
        {
            //Filtrar una lisa de objetos por un rango e valores de una propiedad numérica
            Console.WriteLine("Ejercicio 03:");

            List<Producto> productos = new List<Producto>
        {
            new Producto { Nombre = "Producto1", Precio = 12 },
            new Producto { Nombre = "Producto2", Precio = 15 },
            new Producto { Nombre = "Producto3", Precio = 35 },
            new Producto { Nombre = "Producto4", Precio = 40 },
            new Producto { Nombre = "Producto5", Precio = 46 }
        };

            double precioMinimo = 15;
            double precioMaximo = 35;
            var productosFiltrados = productos.Where(p => p.Precio >= precioMinimo && p.Precio <= precioMaximo).ToList();
            Console.WriteLine("Productos con precio entre " + precioMinimo + " y " + precioMaximo + ":");
            foreach (var producto in productosFiltrados)
            {
                Console.WriteLine(producto.Nombre + ": " + producto.Precio);
            }

        }
        static void Ejercicio04()
        {
            //Filtrar una lista de objetos por un rango de valores de una propiedad numerica
            Console.WriteLine("Ejercicio 04:");

            List<Objeto> objetos = new List<Objeto>
            {
            new Objeto { Valor = 12 },
            new Objeto { Valor = 15 },
            new Objeto { Valor = 35 },
            new Objeto { Valor = 40 },
            new Objeto { Valor = 46 }
            };

            int valorMinimo = 15;
            int valorMaximo = 35;
            var objetosFiltrados = objetos.Where(o => o.Valor >= valorMinimo && o.Valor <= valorMaximo).ToList();
            Console.WriteLine("Objetos con valor entre " + valorMinimo + " y " + valorMaximo + ":");
            foreach (var objeto in objetosFiltrados)
            {
                Console.WriteLine("Valor: " + objeto.Valor);
            }
        }
        static void Ejercicio05()
        {
            //Encuentra la moda en una lista de números
            Console.WriteLine("Ejercicio 05:");
            List<int> numeros = new List<int> { 1, 2, 3, 4, 4, 5, 5, 6, 6, 7, 7 };
            int moda = numeros.GroupBy(n => n)
                             .OrderByDescending(g => g.Count())
                             .Select(g => g.Key)
                             .First();
            Console.WriteLine("La moda es: " + moda);
        }
    }
}
