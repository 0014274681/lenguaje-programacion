﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio02
{
    public class Program
    {

        static void Main(string[] args)
        {
            //calcular la suma de los cuadrados de los numeros en una lista

           //lista de los numeros
            List<int> numeros = new List<int> { 1, 2, 3, 4, 5 ,6};
            //calculmos la suma de los cuadrados
            var sumaCuadrados = numeros.Sum(num => num * num);
           
            //imprimimos los resultados
            Console.WriteLine("La suma de los cuadrados de los números en la lista es: ");
            Console.WriteLine(sumaCuadrados);

        }

    }
}
