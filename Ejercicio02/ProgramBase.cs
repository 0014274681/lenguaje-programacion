﻿using System;
using System.Collections.Generic;

namespace Ejercicio02
{
    public class ProgramBase
    {

        static int Main(string[] args)
        {
            int suma = 0;
            foreach (int num in numeros)
            {
                suma += num * num;
            }
            return suma;
        }

        private static void Main(string[] args)
        {
            // Crear una lista de números
            List<int> numeros = new List<int> { 1, 2, 3, 4, 5 };

            // Calcular la suma de los cuadrados de los números en la lista
            int resultado = SumaCuadrados(numeros);

            // Mostrar el resultado
            Console.WriteLine("La suma de los cuadrados de los números en la lista es: " + resultado);
        }
    }
}